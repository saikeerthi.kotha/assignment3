import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { FormGroup, FormControl} from '@angular/forms'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'assignment3';
  status:boolean=true;

  ngOnInit(): void {
  }

  constructor(private fb:FormBuilder){}
  userForm= this.fb.group({
    mobiles:this.fb.array([
      
    ]),
    
  })

  get mobiles(){
    return this.userForm.get('mobiles') as FormArray;
  }
  addNewMobile(){
    this.mobiles.push(this.fb.control(''))
  }
  
  obj:any={}
  sta:boolean=false;
  phoneno:string;
  arr:any[]=[]
  data(){
    console.log(this.userForm.value.mobiles)
    if(this.userForm.value.mobiles.length>1){
      this.sta=true;
    }
    for(var i=0;i<this.userForm.value.mobiles.length;i++){
      this.obj.phoneNumber=this.userForm.value.mobiles[i]
      console.log(this.obj)
    }
    
    
    
  }

  //delete mobile num
  delete(i){
    console.log(i)
    this.userForm.value.mobiles.splice(i,1)

  }
  
  
}
